/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import LoginPage from './src/pages/LoginPage';
import RegisterPage from "./src/pages/RegisterPage";
import HomePage from "./src/pages/HomePage";
import ProductDetailPage from "./src/pages/ProductDetailPage";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import CategoriesPage from "./src/pages/CategoriesPage";
import CategoryPage from "./src/pages/CategoryPage";
import CartPage from "./src/pages/CartPage";
import CheckoutShippingPage from "./src/pages/CheckoutShippingPage";
import PaymentFormPage from "./src/pages/PaymentFormPage";
import SuccessPage from "./src/pages/SuccessPage";

const CartStack = createNativeStackNavigator();
const CartStackNavigator = () =>{
    return (
        <CartStack.Navigator screenOptions={{headerShown: false}}>
            <CartStack.Screen name="Cart" component={CartPage}/>
            <CartStack.Screen name="CheckoutShipping" component={CheckoutShippingPage}/>
            <CartStack.Screen name="PaymentForm" component={PaymentFormPage}/>
        </CartStack.Navigator>
    )
}

const HomeStack = createNativeStackNavigator();
const HomeStackNavigator = () => {
    return (
        <HomeStack.Navigator screenOptions={{headerShown: false}}>
            <HomeStack.Screen name='Home' component={HomePage}/>
            <HomeStack.Screen name="ProductDetail" component={ProductDetailPage}/>
        </HomeStack.Navigator>
    )
}
const CategoriesStack = createNativeStackNavigator();
const CategoriesStackNavigator = () => {
    return (
        <CategoriesStack.Navigator screenOptions={{headerShown: false}}>
            <CategoriesStack.Screen name='Categories' component={CategoriesPage}/>
            <CategoriesStack.Screen name="Category" component={CategoryPage}/>
        </CategoriesStack.Navigator>
    )
}

const Tab = createBottomTabNavigator();
const TabNavigation = () => {
    return (
        <Tab.Navigator screenOptions={{
            headerShown: false,
            tabBarIconStyle: {display: 'none'},
            tabBarStyle: [{display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}, null],
            tabBarLabelStyle: {fontSize: 15}
        }}>
            <Tab.Screen name='Home' component={HomeStackNavigator}/>
            <Tab.Screen options={{ unmountOnBlur:true }} name='Cart' component={CartStackNavigator}/>
            <Tab.Screen name='Categories' component={CategoriesStackNavigator}/>
        </Tab.Navigator>
    )
}

const Stack = createNativeStackNavigator();
export default App = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{headerShown: false}} initialRouteName='Login'>
                <Stack.Screen name="Login" component={LoginPage}/>
                <Stack.Screen name="Register" component={RegisterPage}/>
                <Stack.Screen name="Success" component={SuccessPage}/>
                <Stack.Screen name='Home' component={TabNavigation}/>
            </Stack.Navigator>
        </NavigationContainer>
    );
};
