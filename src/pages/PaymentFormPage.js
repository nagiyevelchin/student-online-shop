import React, {Component} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {ScrollView, StatusBar, StyleSheet, View} from 'react-native';
import CustomHeader from "../components/CustomHeader";
import CustomInput from "../components/CustomInput";
import CustomButton from "../components/CustomButton";


export default class PaymentFormPage extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <SafeAreaView style={{backgroundColor: '#fff'}}>
                <StatusBar barStyle='dark-content' backgroundColor='white'/>
                <ScrollView style={styles.container} stickyHeaderIndices={[0]} contentContainerStyle={{paddingBottom: 20}}>
                    <CustomHeader navigation={this.props.navigation} title='Payment info'/>
                    <View style={{paddingHorizontal: 20, marginTop: 20, flex: 1, justifyContent: 'flex-start', alignItems: 'stretch'}}>
                        <CustomInput style={{width: '100%'}} placeholder='Enter Card Number'/>
                        <View style={{flexDirection: 'row', marginTop: 20, justifyContent: 'space-between'}}>
                            <CustomInput style={{width: '90%'}} placeholder='Expiration Date'/>
                            <CustomInput style={{width: '90%'}} placeholder='Security Code'/>
                        </View>
                        <CustomInput style={{width: '100%', marginTop: 15}} placeholder='Enter Card Holder'/>

                        <CustomButton onPress={()=>{this.props.navigation.navigate('Success')}} title='Check Out' style={{marginTop: 10}}/>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        backgroundColor: '#fff'
    },
    tableContainer: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        padding: 15,
        borderRadius: 8,
        borderWidth: 1,
        borderColor: '#ebf0ff',
        marginTop: 15
    },
    tableRow: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 10
    },
    tableItemTitle: {
        color: '#9098b1',
        fontSize: 15
    },
    tableItemValue: {
        color: '#223263',
        fontSize: 15
    }
});
