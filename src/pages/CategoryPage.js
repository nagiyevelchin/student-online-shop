import React, {Component} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {ScrollView, StatusBar, StyleSheet, View} from 'react-native';
import ProductGroupsBig from "../components/ProductGroupsBig";
import CustomHeader from "../components/CustomHeader";


export default class CategoryPage extends Component {

    constructor(props) {
        super(props);
    }

    state = {
        products:[
            {
                image:require('../../assets/prds/acer_chrome.webp'),
                title:'Acer - Chromebook 311 – 11.6” HD Display'
            },
            {
                image:require('../../assets/prds/samsung-galaxy.webp'),
                title:'Samsung - Galaxy Book 15.6" LED Touch Screen - Intel® Core™ i5-1165G7 -8GB Memory'
            },
            {
                image:require('../../assets/prds/hp.webp'),
                title:'HP - 15.6" Touch-Screen Laptops - AMD Ryzen 3 - 8GB Memory - 256GB SSD - Natural'
            },
            {
                image:require('../../assets/prds/iBUYPOWER.webp'),
                title:'iBUYPOWER - Slate MR Gaming Desktop - Intel i3 10105F 8 GB Memory - NVIDIA'
            },
            {
                image:require('../../assets/prds/HP-OMEN.webp'),
                title:'HP OMEN - 25L Gaming Desktop - 10th Generation Intel Core i5-10400 - 8GB Memory'
            },
            {
                image:require('../../assets/prds/HP-OMEN-2.webp'),
                title:'HP OMEN - 30L Gaming Desktop - AMD Ryzen 5 5600G - 16GB Memory - NVIDIA® GeForce'
            },
            {
                image:require('../../assets/prds/ency.webp'),
                title:'HP - ENVY Photo 7855 Wireless All-In-One Instant Ink Ready Inkjet Printer'
            },
            {
                image:require('../../assets/prds/office_pro.webp'),
                title:'HP - OfficeJet Pro 8035e Wireless All-In-One Inkjet Printer with up to 12 months'
            },
            {
                image:require('../../assets/prds/OfficeJet.webp'),
                title:'HP - OfficeJet Pro 8025e Wireless All-In-One Inkjet'
            },
            {
                image:require('../../assets/prds/Epson-WorkForce.webp'),
                title:'Epson - WorkForce ES-400 II Duplex Desktop Document'
            },
            {
                image:require('../../assets/prds/Brother-ADS.webp'),
                title:'Brother - ADS-1700W Wireless Desktop Document Scanner with Touchscreen LCD'
            },
            {
                image:require('../../assets/prds/Brother-DS-940DW.webp'),
                title:'Brother - DS-940DW Compact Wireless Duplex Mobile Document Scanner - White'
            }
        ]
    }

    render() {
        return (
            <SafeAreaView style={{backgroundColor: '#fff'}}>
                <StatusBar barStyle='dark-content' backgroundColor='white'/>
                <ScrollView style={styles.container} stickyHeaderIndices={[0]} contentContainerStyle={{paddingBottom: 20}}>
                    <CustomHeader navigation={this.props.navigation} title='Shirt'/>
                    <View style={{paddingLeft: 20}}>
                        <ProductGroupsBig products={this.state.products} hide_image={true} navigation={this.props.navigation} style={{marginTop: 20}}/>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        backgroundColor: '#fff'
    },
    header: {
        color: '#223263',
        fontWeight: 'bold',
        lineHeight: 28,
        fontSize: 20
    },
    subTitle: {
        fontSize: 14,
        marginTop: 8,
        lineHeight: 25,
        color: '#9098B1'
    }
});
