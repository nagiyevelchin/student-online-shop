import React, {Component} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Image, StatusBar, StyleSheet, Text, View} from 'react-native';
import CustomButton from "../components/CustomButton";


export default class SuccessPage extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <SafeAreaView style={{backgroundColor: '#fff'}}>
                <StatusBar barStyle='dark-content' backgroundColor='white'/>
                <View style={{justifyContent: 'center', alignItems: 'center', height: '100%', paddingHorizontal: 20}}>
                    <Image source={require('../../assets/icons/Success.png')}/>
                    <Text style={{color: '#223263', fontSize: 25, fontWeight: 'bold', marginTop: 20}}>Success</Text>
                    <Text style={{color: '#223263', opacity: 0.5, fontSize: 14, fontWeight: 'bold', marginTop: 15}}>thank you for shopping using Shop</Text>
                    <CustomButton onPress={() => {
                        this.props.navigation.navigate('Home', {screen:'Home'})
                    }} title='Back to Home' style={{width: '100%', marginTop: 15}}/>
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        backgroundColor: '#fff'
    },
    tableContainer: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        padding: 15,
        borderRadius: 8,
        borderWidth: 1,
        borderColor: '#ebf0ff',
        marginTop: 15
    },
    tableRow: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 10
    },
    tableItemTitle: {
        color: '#9098b1',
        fontSize: 15
    },
    tableItemValue: {
        color: '#223263',
        fontSize: 15
    }
});
