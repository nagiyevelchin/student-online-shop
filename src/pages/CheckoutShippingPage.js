import React, {Component} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {ScrollView, StatusBar, StyleSheet, View} from 'react-native';
import CustomHeader from "../components/CustomHeader";
import CustomButton from "../components/CustomButton";
import ShippingAddress from "../components/ShippingAddress";


export default class CheckoutShippingPage extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <SafeAreaView style={{backgroundColor: '#fff'}}>
                <StatusBar barStyle='dark-content' backgroundColor='white'/>
                <ScrollView style={styles.container} stickyHeaderIndices={[0]} contentContainerStyle={{paddingBottom: 20}}>
                    <CustomHeader navigation={this.props.navigation} title='Ship To'/>
                    <View style={{paddingHorizontal: 20}}>
                        <ShippingAddress title='Priscekila' active={true}/>
                        <ShippingAddress title='Ahmad Khaidir' style={{marginTop: 10}}/>
                        <CustomButton onPress={()=>{this.props.navigation.navigate('PaymentForm')}} title='Next' style={{marginTop: 15}}/>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        backgroundColor: '#fff'
    },
    tableContainer: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        padding: 15,
        borderRadius: 8,
        borderWidth: 1,
        borderColor: '#ebf0ff',
        marginTop: 15
    },
    tableRow: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 10
    },
    tableItemTitle: {
        color: '#9098b1',
        fontSize: 15
    },
    tableItemValue: {
        color: '#223263',
        fontSize: 15
    }
});
