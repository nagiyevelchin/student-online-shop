import React from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {ScrollView, StatusBar, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import CustomInput from "../components/CustomInput";
import CustomButton from "../components/CustomButton";
import TextButton from "../components/TextButton";
import {CommonActions} from "@react-navigation/native";


export default LoginPage = ({navigation}) => {

    const login = () => {
        navigation.dispatch(
            CommonActions.reset({
                index: 1,
                routes: [
                    { name: 'Home' },
                ],
            }),
        );
    }

    const goToRegister = () => {
        navigation.navigate('Register');
    }

    return (
        <SafeAreaView style={{backgroundColor: '#fff'}}>
            <StatusBar barStyle='light-content'/>
            <ScrollView style={styles.container}>
                <View style={{alignItems: 'center'}}>
                    <Text style={styles.header}>
                        Welcome to Shop
                    </Text>
                    <Text>
                        Sign in to continue
                    </Text>
                </View>
                <CustomInput style={{marginTop: 28}} placeholder='Your Email'/>
                <CustomInput style={{marginTop: 8}} placeholder='Your Password'/>
                <CustomButton onPress={login} style={{marginTop: 20}} title='Sign In'/>

                <Text style={[styles.subTitle, {alignSelf: 'center'}]}>
                    Don’t have a account? <TextButton onPress={goToRegister} title='Register'/>
                </Text>
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        paddingTop: 120,
        paddingHorizontal: 20,
        backgroundColor: '#fff'
    },
    header: {
        color: '#223263',
        fontWeight: 'bold',
        lineHeight: 28,
        fontSize: 20
    },
    subTitle: {
        fontSize: 14,
        marginTop: 8,
        lineHeight: 25,
        color: '#9098B1'
    }
});
