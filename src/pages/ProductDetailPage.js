import React, {Component} from 'react';
import {SafeAreaView} from "react-native-safe-area-context";
import {ScrollView, StatusBar, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {SliderBox} from "react-native-image-slider-box";
import CustomPicker from "../components/CustomPicker";
import ProductGroups from "../components/ProductGroups";
import CustomButton from "../components/CustomButton";
import CustomHeader from "../components/CustomHeader";

export default class ProductDetailPage extends Component {
    constructor(props) {
        super(props);
    }

    state = {
        sizes: [
            {label: '6', value: '6'},
            {label: '6.5', value: '6.5'},
            {label: '7', value: '7'},
            {label: '7.5', value: '7.5'},
            {label: '8', value: '8'},
            {label: '8.5', value: '8.5'},
        ],
        size: 6,
        colors: [
            {label: 'White', value: 'White'},
            {label: 'Yellow', value: 'Yellow'},
            {label: 'Blue', value: 'Blue'},
            {label: 'Green', value: 'Green'}
        ],
        color: 'White',
        products:[
            {
                image:require('../../assets/prds/acer_chrome.webp'),
                title:'Acer - Chromebook 311 – 11.6” HD Display'
            },
            {
                image:require('../../assets/prds/samsung-galaxy.webp'),
                title:'Samsung - Galaxy Book 15.6" LED Touch Screen - Intel® Core™ i5-1165G7 -8GB Memory'
            },
            {
                image:require('../../assets/prds/hp.webp'),
                title:'HP - 15.6" Touch-Screen Laptops - AMD Ryzen 3 - 8GB Memory - 256GB SSD - Natural'
            },
            {
                image:require('../../assets/prds/iBUYPOWER.webp'),
                title:'iBUYPOWER - Slate MR Gaming Desktop - Intel i3 10105F 8 GB Memory - NVIDIA'
            },
            {
                image:require('../../assets/prds/HP-OMEN.webp'),
                title:'HP OMEN - 25L Gaming Desktop - 10th Generation Intel Core i5-10400 - 8GB Memory'
            },
            {
                image:require('../../assets/prds/HP-OMEN-2.webp'),
                title:'HP OMEN - 30L Gaming Desktop - AMD Ryzen 5 5600G - 16GB Memory - NVIDIA® GeForce'
            },
            {
                image:require('../../assets/prds/ency.webp'),
                title:'HP - ENVY Photo 7855 Wireless All-In-One Instant Ink Ready Inkjet Printer'
            },
            {
                image:require('../../assets/prds/office_pro.webp'),
                title:'HP - OfficeJet Pro 8035e Wireless All-In-One Inkjet Printer with up to 12 months'
            },
            {
                image:require('../../assets/prds/OfficeJet.webp'),
                title:'HP - OfficeJet Pro 8025e Wireless All-In-One Inkjet'
            },
            {
                image:require('../../assets/prds/Epson-WorkForce.webp'),
                title:'Epson - WorkForce ES-400 II Duplex Desktop Document'
            },
            {
                image:require('../../assets/prds/Brother-ADS.webp'),
                title:'Brother - ADS-1700W Wireless Desktop Document Scanner with Touchscreen LCD'
            },
            {
                image:require('../../assets/prds/Brother-DS-940DW.webp'),
                title:'Brother - DS-940DW Compact Wireless Duplex Mobile Document Scanner - White'
            }
        ]
    }

    render() {
        return (
            <SafeAreaView style={{backgroundColor: '#fff'}}>
                <StatusBar barStyle='dark-content' backgroundColor='white'/>
                <ScrollView style={styles.container} stickyHeaderIndices={[0]} contentContainerStyle={{paddingBottom: 20}}>
                    <CustomHeader navigation={this.props.navigation} title={this.props.route.params.title}/>
                    <SliderBox dotColor='#40BFFF' images={[this.props.route.params.image]} ImageComponentStyle={{ height: 250 }}/>
                    <View style={{paddingHorizontal: 20}}>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 15, backgroundColor:'#fff'}}>
                            <Text style={{color: '#223263', flex:1, fontSize: 15, lineHeight: 20, fontWeight: 'bold'}}>{this.props.route.params.title}</Text>
                            <TouchableOpacity onPress={()=>{}}>
                                <Text style={{color: 'red', fontSize: 14, fontWeight: 'bold', lineHeight: 28}}>+Wishlist</Text>
                            </TouchableOpacity>
                        </View>
                        <Text style={{color: '#40bfff', fontWeight: 'bold', fontSize: 16, marginTop: 8}}>$299,43</Text>

                        <CustomPicker label='Select size' style={{marginTop: 15}} items={this.state.sizes} selectedValue={this.state.size} onValueChange={(value, index) => {
                            this.setState({
                                size: value
                            })
                        }}/>
                        <CustomPicker label='Select color' style={{marginTop: 15}} items={this.state.colors} selectedValue={this.state.color} onValueChange={(value, index) => {
                            this.setState({
                                color: value
                            })
                        }}/>

                        <Text style={[styles.title, {marginTop: 15}]}>Specification</Text>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 10}}>
                            <Text style={{fontSize: 14, color: '#223263'}}>Shown:</Text>
                            <Text style={{textAlign: 'right', color: '#9098b1', fontWeight: '400'}}>{"Laser\r\n Blue/Anthracite/Watermel\r\non/White"}</Text>
                        </View>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 15}}>
                            <Text style={{fontSize: 14, color: '#223263'}}>Style:</Text>
                            <Text style={{textAlign: 'right', color: '#9098b1', fontWeight: '400'}}>CD0113-400</Text>
                        </View>
                        <Text style={{ color: '#9098b1', fontWeight: '400', marginTop:20}}>
                            The Nike Air Max 270 React ENG combines a full-length React foam midsole with a 270 Max Air unit for unrivaled comfort and a striking visual experience.
                        </Text>
                    </View>
                    <ProductGroups products={this.state.products} navigation={this.props.navigation} title='You Might Also Like' style={{marginTop: 25}}/>
                    <View style={{ paddingHorizontal:20, marginTop:25 }}>
                        <CustomButton title={'Add To Cart'}/>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        backgroundColor: '#fff'
    },
    header: {
        color: '#223263',
        fontWeight: 'bold',
        fontSize: 17
    },
    subTitle: {
        fontSize: 14,
        marginTop: 8,
        lineHeight: 25,
        color: '#9098b1'
    },
    title: {color: '#223263', fontSize: 15, fontWeight: 'bold'}
});
