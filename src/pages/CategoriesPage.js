import React, {Component} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {ScrollView, StatusBar, StyleSheet, Text, TouchableOpacity} from 'react-native';
import CustomHeader from "../components/CustomHeader";


export default class CategoriesPage extends Component {

    MenuItem = ({title, active}) =>{
        return (
            <TouchableOpacity onPress={()=>this.props.navigation.navigate('Category')} style={{paddingHorizontal: 20, paddingVertical:20}}>
                <Text style={{fontSize: 15, color: active ? '#40bfff' : '#223263', fontWeight: 'bold'}}>
                    {title}
                </Text>
            </TouchableOpacity>
        )
    }

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <SafeAreaView style={{backgroundColor: '#fff'}}>
                <StatusBar barStyle='dark-content' backgroundColor='white'/>
                <ScrollView style={styles.container} stickyHeaderIndices={[0]} contentContainerStyle={{paddingVertical: 20}}>
                    <CustomHeader navigation={this.props.navigation} title='Categories'/>
                    <this.MenuItem title='Shirt' active={true}/>
                    <this.MenuItem title='Bikini'/>
                    <this.MenuItem title='Dress'/>
                    <this.MenuItem title='Work Equipment'/>
                    <this.MenuItem title='Man Pants'/>
                    <this.MenuItem title='Man Shoes'/>
                    <this.MenuItem title='Man Underwear'/>
                    <this.MenuItem title='Man T-Shirt'/>
                    <this.MenuItem title='Woman Bag'/>
                    <this.MenuItem title='Woman Pants'/>
                    <this.MenuItem title='High Heels'/>
                    <this.MenuItem title='Woman T-Shirt'/>
                    <this.MenuItem title='Skirt'/>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        backgroundColor: '#fff'
    },
    header: {
        color: '#223263',
        fontWeight: 'bold',
        lineHeight: 28,
        fontSize: 20
    },
    subTitle: {
        fontSize: 14,
        marginTop: 8,
        lineHeight: 25,
        color: '#9098B1'
    }
});
