import React from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {ScrollView, StatusBar, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import CustomInput from "../components/CustomInput";
import CustomButton from "../components/CustomButton";
import TextButton from "../components/TextButton";
import {CommonActions} from "@react-navigation/native";


export default RegisterPage = ({navigation}) => {

    const register = () => {
        navigation.dispatch(
            CommonActions.reset({
                index: 1,
                routes: [
                    { name: 'Home' },
                ],
            }),
        );
    }

    const goToLogin = () => {
        navigation.navigate('Login');
    }

    return (
        <SafeAreaView style={{backgroundColor: '#fff'}}>
            <StatusBar barStyle='light-content'/>
            <ScrollView style={styles.container}>
                <View style={{alignItems: 'center'}}>
                    <Text style={styles.header}>
                        Let’s Get Started
                    </Text>
                    <Text>
                        Create an new account
                    </Text>
                </View>
                <CustomInput style={{marginTop: 28}} placeholder='Full Name'/>
                <CustomInput style={{marginTop: 8}} placeholder='Your Email'/>
                <CustomInput password={true} style={{marginTop: 8}} placeholder='Password'/>
                <CustomInput password={true} style={{marginTop: 8}} placeholder='Password Again'/>
                <CustomButton onPress={register} style={{marginTop: 20}} title='Sign Up'/>

                <Text style={[styles.subTitle, {alignSelf: 'center'}]}>
                    have a account? <TextButton onPress={goToLogin} title='Sign in'/>
                </Text>
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        paddingTop: 120,
        paddingHorizontal: 20,
        backgroundColor: '#fff'
    },
    header: {
        color: '#223263',
        fontWeight: 'bold',
        lineHeight: 28,
        fontSize: 20
    },
    subTitle: {
        fontSize: 14,
        marginTop: 8,
        lineHeight: 25,
        color: '#9098B1'
    }
});
