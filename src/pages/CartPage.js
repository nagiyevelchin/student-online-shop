import React, {Component} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {ScrollView, StatusBar, StyleSheet, Text, View} from 'react-native';
import CustomHeader from "../components/CustomHeader";
import BasketProduct from "../components/BasketProduct";
import CustomButton from "../components/CustomButton";


export default class CartPage extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <SafeAreaView style={{backgroundColor: '#fff'}}>
                <StatusBar barStyle='dark-content' backgroundColor='white'/>
                <ScrollView style={styles.container} stickyHeaderIndices={[0]} contentContainerStyle={{paddingBottom: 20}}>
                    <CustomHeader hideBackButton={true} navigation={this.props.navigation} title='Your Cart'/>
                    <View style={{paddingHorizontal: 20}}>
                        <BasketProduct wishlist={true}/>
                        <BasketProduct style={{marginTop: 15}}/>
                        <View style={styles.tableContainer}>
                            <View style={styles.tableRow}>
                                <Text style={styles.tableItemTitle}>Items (2)</Text>
                                <Text style={styles.tableItemValue}>$598.86</Text>
                            </View>
                            <View style={styles.tableRow}>
                                <Text style={styles.tableItemTitle}>Shipping</Text>
                                <Text style={styles.tableItemValue}>$40</Text>
                            </View>
                            <View style={styles.tableRow}>
                                <Text style={styles.tableItemTitle}>Import charges</Text>
                                <Text style={styles.tableItemValue}>$128</Text>
                            </View>
                            <View style={styles.tableRow}>
                                <View style={{borderBottomWidth: 1, borderBottomColor: '#ebf0ff', borderStyle: 'dashed', width: '100%'}}/>
                            </View>

                            <View style={styles.tableRow}>
                                <Text style={{color: '#223263', fontSize: 16, fontWeight: 'bold'}}>Total price</Text>
                                <Text style={{color: '#40bfff', fontWeight: 'bold'}}>$766.86</Text>
                            </View>
                        </View>

                        <CustomButton onPress={()=>this.props.navigation.navigate('CheckoutShipping')} title='Check Out' style={{marginTop: 15}}/>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        backgroundColor: '#fff'
    },
    tableContainer: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        padding: 15,
        borderRadius: 8,
        borderWidth: 1,
        borderColor: '#ebf0ff',
        marginTop: 15
    },
    tableRow: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 10
    },
    tableItemTitle: {
        color: '#9098b1',
        fontSize: 15
    },
    tableItemValue: {
        color: '#223263',
        fontSize: 15
    }
});
