import React, {Component} from "react";
import {Image, Text, TouchableOpacity, View} from "react-native";

export default class BasketProduct extends Component {
    state = {
        count:1
    }

    increment = () => {
        this.setState({
            count:++this.state.count
        });
    }

    decrement = () => {
        if (this.state.count === 1){
            return;
        }
        this.setState({
            count:--this.state.count
        });
    }

    render() {
        return (
            <View style={[this.props.style, {flexDirection: 'row', paddingHorizontal: 15, paddingVertical: 10, borderWidth: 1, borderRadius: 8, borderColor: '#ebf0ff'}]}>
                <Image source={require('../../assets/prds/prd1.png')} style={{width: 100, aspectRatio: 1}} resizeMethod='scale'/>
                <View style={{justifyContent: 'space-between', paddingLeft: 10, flex: 1}}>
                    <View style={{flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start'}}>
                        <Text style={{color: '#223263', fontSize: 14, fontWeight: 'bold', flex: 1}}>Nike Air Zoom Pegasus 36 Miami</Text>
                        {this.props.wishlist && <Image source={require('../../assets/icons/heart_active.png')} style={{marginLeft: 5, width: 23, aspectRatio: 1}}/>
                        || <Image source={require('../../assets/icons/heart.png')} style={{marginLeft: 5, width: 23, aspectRatio: 1}}/>}
                        <Image source={require('../../assets/icons/trash.png')} style={{marginLeft: 5, width: 23, aspectRatio: 1}}/>
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                        <Text style={{color: '#40bfff', fontWeight: 'bold', fontSize: 16}}>$299,43</Text>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <TouchableOpacity onPress={this.decrement} style={{
                                width: 40,
                                height: 30,
                                borderTopLeftRadius: 5,
                                borderBottomLeftRadius: 5,
                                borderWidth: 1,
                                borderColor: '#ebf0ff',
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}><Text style={{color: '#9098b1', fontWeight: 'bold', fontSize: 20}}>-</Text></TouchableOpacity>
                            <View style={{backgroundColor: '#ebf0ff', width: 30, height: 30, alignItems: 'center', justifyContent: 'center'}}><Text>{this.state.count}</Text></View>
                            <TouchableOpacity onPress={this.increment} style={{
                                width: 40,
                                height: 30,
                                borderTopRightRadius: 5,
                                borderBottomRightRadius: 5,
                                borderWidth: 1,
                                borderColor: '#ebf0ff',
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}><Text style={{color: '#9098b1', fontWeight: 'bold', fontSize: 20}}>+</Text></TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}
