import React, {Component} from 'react';
import {Text, TouchableOpacity} from "react-native";


class TextButton extends Component {
    render() {
        return (
            <Text onPress={this.props.onPress} style={[this.props.textStyle,{
                color: '#40BFFF',
            }]}>
                {this.props.title}
            </Text>
        );
    }
}

export default TextButton;
