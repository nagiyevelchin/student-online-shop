import React, {Component} from 'react';
import {Image, TouchableOpacity, View} from "react-native";
import Product from "./Product";

export default class ProductGroupsBig extends Component {
    render() {
        return (
            <View style={[this.props.style, {paddingRight: 20}]}>
                {this.props.hide_image || <TouchableOpacity>
                    <Image style={{width: '100%', borderRadius: 10}} source={require('../../assets/images/recommend_banner.png')}/>
                </TouchableOpacity>}
                <View style={{flexDirection: 'row', justifyContent:'space-between', flexWrap: 'wrap'}}>
                    {
                        this.props.products.map((item, index)=>{
                            return (
                                <Product key={index} navigation={this.props.navigation} product={item} style={{width: '48%', marginTop:15 }}/>
                            );
                        })
                    }
                </View>
            </View>
        )
    }
}
