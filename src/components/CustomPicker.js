import React, {Component} from 'react';
import {Text, View} from "react-native";
import {Picker} from "@react-native-picker/picker";

export default class CustomPicker extends Component {
    render() {
        return (
            <View style={this.props.style}>
                <Text style={{color: '#223263', fontSize: 15 , fontWeight: 'bold'}}>{this.props.label}</Text>
                <View style={{
                    height: 48,
                    borderColor: '#ebf0ff',
                    borderWidth: 1,
                    borderRadius: 5,
                    justifyContent: 'center',
                    marginTop:8
                }}>
                    <Picker dropdownIconColor='#40BFFF' selectedValue={this.props.selectedValue} onValueChange={this.props.onValueChange}>
                        {this.props.items.map((item, index)=>
                            <Picker.Item key={index} label={item.label} value={item.value}/>)}
                    </Picker>
                </View>
            </View>
        )
    }
}
