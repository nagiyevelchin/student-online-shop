import React, {Component} from "react";
import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";

export default class CustomHeader extends Component {

    render() {
        return (
            <View style={{justifyContent: 'flex-start', alignItems: 'center', flexDirection: 'row', paddingLeft:this.props.hideBackButton ? 20 : 0,  paddingVertical:15, marginRight:20, backgroundColor: '#fff'}}>
                {this.props.hideBackButton || <TouchableOpacity onPress={()=>this.props.navigation.goBack()} style={{width: 40, height:30, marginRight: 15, justifyContent:'center', alignItems:'center'}}>
                    <Image source={require('../../assets/icons/left.png')} resizeMethod='resize' style={{height:14, aspectRatio:0.5}}/>
                </TouchableOpacity>}
                <Text numberOfLines={1} style={styles.header}>
                    {this.props.title}
                </Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        color: '#223263',
        fontWeight: 'bold',
        fontSize: 15,
        flex:1
    }
});
