import React, {Component} from 'react';
import {Text, TouchableOpacity} from "react-native";


class CustomButton extends Component {
    render() {
        return (
            <TouchableOpacity onPress={this.props.onPress} style={[this.props.style, {
                backgroundColor: '#40BFFF',
                borderRadius: 10,
                height: 57,
                justifyContent: 'center',
                alignItems: 'center'
            }]}>
                <Text style={{
                    color: 'white',
                    fontSize: 15,
                    fontWeight: 'bold'
                }}>{this.props.title}</Text>
            </TouchableOpacity>
        );
    }
}

export default CustomButton;
