import React, {Component} from 'react';
import {ScrollView, Text, TouchableOpacity, View} from "react-native";
import Product from "./Product";

export default class ProductGroups extends Component {
    render() {
        return (
            <View style={[this.props.style]}>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 20}}>
                    <Text style={{fontWeight: 'bold', color: '#223263', fontSize: 16}}>{this.props.title}</Text>
                    <TouchableOpacity onPress={this.onLinkClick}>
                        <Text style={{fontWeight: 'bold', color: '#40bfff', fontSize: 16}}>{this.props.link_title}</Text>
                    </TouchableOpacity>
                </View>
                <ScrollView contentContainerStyle={{ paddingHorizontal:20 }} showsHorizontalScrollIndicator={false} horizontal={true} style={{marginTop: 15}}>
                    {
                        this.props.products.map((item, index)=>{
                            return (
                                <Product key={index} product={item} navigation={this.props.navigation} style={{maxWidth: 160, marginRight: index == this.props.products.length - 1 ? 0 : 15}}/>
                            );
                        })
                    }
                </ScrollView>
            </View>
        )
    }
}
