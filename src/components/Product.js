import React, {Component} from 'react';
import {Image, Text, TouchableOpacity, View} from "react-native";

export default class Product extends Component {

    goToDetail = () =>{
        this.props.navigation.navigate('ProductDetail', {
            title:this.props.product ? this.props.product.title : 'Nike Air Zoom Pegasus 36 Miami',
            image:this.props.product ? this.props.product.image : require('../../assets/prds/big_prd_1.png'),
        });
    }

    render() {
        return (
            <TouchableOpacity onPress={this.goToDetail} style={[{
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                borderWidth: 1,
                borderColor: '#ebf0ff',
                paddingHorizontal: 10,
                paddingBottom: 15,
                paddingTop: 10
            }, this.props.style]}>
                <Image resizeMode='center' style={{borderRadius: 5, alignSelf:'center', width:'100%', height:100}} source={this.props.product ? this.props.product.image : this.props.image}/>
                <Text numberOfLines={2} style={{fontSize: 13, color: '#223263', fontWeight: 'bold', marginTop: 5}}>{this.props.product ? this.props.product.title : 'FS - Nike Air Max 270 React'}</Text>
                <Text style={{color: '#40bfff', fontWeight: 'bold', marginTop: 8}}>$299,43</Text>
                <View style={{flexDirection: 'row', marginTop: 8}}>
                    <Text style={{color: '#9098b1', fontSize: 11, textDecorationLine: 'line-through'}}>$534,33</Text>
                    <Text style={{color: '#fb7181', fontWeight: 'bold', fontSize: 11, marginLeft: 4}}>24% Off</Text>
                </View>
            </TouchableOpacity>
        )
    }
}
