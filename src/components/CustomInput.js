import React, {Component} from 'react';
import {TextInput, View} from "react-native";


class CustomInput extends Component {
    state = {
        focused:false
    }

    setFocused = (focused) =>{
        this.setState({
            focused:focused
        });
    }

    render() {
        return (
            <View style={{ flex:1 }}>
                <TextInput onFocus={()=>{this.setFocused(true)}} onBlur={()=>{this.setFocused(false)}} secureTextEntry={!!this.props.password} placeholder={this.props.placeholder} style={[this.props.style, {
                    height: 48,
                    borderColor: this.state.focused ? '#40BFFF' :'#ebf0ff',
                    borderWidth: 1,
                    paddingLeft: 20,
                    borderRadius:5
                }]} placeholderTextColor='#9098B1'>

                </TextInput>
            </View>
        );
    }
}

export default CustomInput;
