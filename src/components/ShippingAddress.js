import React, {Component} from "react";
import {Image, Text, View} from "react-native";
import CustomButton from "./CustomButton";


export default class ShippingAddress extends Component {


    render() {
        return (
            <View style={[this.props.style, {padding: 20, flexDirection: 'column', borderRadius: 8, borderWidth: 1, borderColor: this.props.active ? '#40bfff': '#EBF0FF'}]}>
                <Text style={{color: '#223263', fontSize: 16, fontWeight: 'bold'}}>{this.props.title}</Text>
                <Text style={{marginTop: 15, fontWeight: '400', color: '#9098b1', fontSize: 14, lineHeight: 24}}>3711 Spring Hill Rd undefined Tallahassee, Nevada 52874 United States</Text>
                <Text style={{marginTop: 15, fontWeight: '400', color: '#9098b1', fontSize: 14, lineHeight: 24}}>+99 1234567890</Text>
                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
                    <CustomButton style={{width: '30%'}} title='Edit'/>
                    <Image source={require('../../assets/icons/trash.png')} style={{marginLeft: 30, width: 25, aspectRatio: 1}}/>
                </View>
            </View>
        )
    }
}
